import os
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.transforms as mtransforms
import warnings

plt.rcParams.update({'font.size': 14})

def abs2(x):
    return x.real**2 + x.imag**2


class AtomicUnits:
    """Class storing atomic units.

    All variables, arrays in simulations are in atomic units.

    Attributes
    ----------
    Eh : float
        Hartree energy (in meV)
    Ah : float
        Bohr radius (in nanometers)
    Th : float
        time (in picoseconds)
    Bh : float
        magnetic induction (in Teslas)
    """
    # atomic units
    Eh=27211.4 # meV
    Ah=0.05292 # nm
    Th=2.41888e-5 # ps
    Bh=235051.76 # Teslas

au = AtomicUnits()


class QWmaterial:
    """ Class containing material parameters.

    Attributes
    ----------
    m0 : float
        effective band mass
    """
    def __init__(self, dim, m0, gfactor):
        self.dim = dim  # Hamiltoniam dimension, at the end it will be 4 (x 4)
        self.m0 = m0
        self.gfactor = gfactor
        
WS2 = QWmaterial(2, 0.39, -0.44)
WSe2 = QWmaterial(2, 0.45, -0.44)


class DeviceParameters:
    """ Class containing device parameters.

    Attributes
    ----------
    ...

    """
    
    # schroedinger 2d grid geometry
    grid_x_dim = 71
    grid_y_dim = 71
    grid_dx = 0.2/au.Ah
    grid_dy = 0.2/au.Ah
    grid_x_position = 28./au.Ah  # schroedinger computational grid relative position (in poisson box coordinates)
    grid_y_position = 28./au.Ah
    grid_x_space = np.linspace(0, grid_dx*(grid_x_dim-1), grid_x_dim, endpoint=True)
    grid_x_space += grid_x_position
    grid_y_space = np.linspace(0, grid_dy*(grid_y_dim-1), grid_y_dim, endpoint=True)
    grid_y_space += grid_y_position

    # poisson computational box geometry
    box_x_size = 70./au.Ah  #
    box_y_size = 70./au.Ah  #
    box_z_size = 30./au.Ah  #
    
    mesh_x_size = int(box_x_size*au.Ah)
    mesh_y_size = int(box_y_size*au.Ah)
    mesh_z_size = int(box_z_size*au.Ah)

    # grid when saving/loading potential
    save_grid_start = np.array([200., 170., 300.])/au.Ah 
    save_grid_end = np.array([800., 230., 320.])/au.Ah
    save_grid_points = [70, 70, 30]
    save_grid_x_space = np.linspace(save_grid_start[0], save_grid_end[0], save_grid_points[0]+1, endpoint=True)
    save_grid_y_space = np.linspace(save_grid_start[1], save_grid_end[1], save_grid_points[1]+1, endpoint=True)
    save_grid_z_space = np.linspace(save_grid_start[2], save_grid_end[2], save_grid_points[2]+1, endpoint=True)

    # gates z-position
    gates_z = 25./au.Ah

    # monolayer geometry
    layer_z = 20./au.Ah  #
    layer_width = 1./au.Ah  #
    sigma_z = 1./au.Ah

    # bottom barrier
    barrier_z = 285./au.Ah

    # permittivities
    emos2 = 6.4  # <- perpendicular, 15.3 <- parallel (anisotropy!)
    ews2 = 6.3  # 13.7 # in-plane
    ewse2 = 7.5 # 7.5  # 15.5 # in-plane
    ehbn = 3.3 # 3.3  # 6.9  # in-plane
    esio2 = 3.9
    ehfo2 = 20.

class Plotting:

    def __init__(self, grid_size=[101,101,101], device=None, directory=None):
        self.dev = device
        self.xx = np.linspace(0, self.dev.box_x_size, num=grid_size[0], endpoint=True)[::-1]
        self.yy = np.linspace(0, self.dev.box_y_size, num=grid_size[1], endpoint=True)[::-1]
        self.zz = np.linspace(0, self.dev.box_z_size, num=grid_size[2], endpoint=True)[::-1]
        self.x_points = np.linspace(1, self.dev.box_x_size, grid_size[0], endpoint=True)
        self.y_points = np.linspace(1, self.dev.box_y_size, grid_size[1], endpoint=True)
        self.z_points = np.linspace(1, self.dev.box_z_size, grid_size[2], endpoint=True)
        if directory is not None:
            self.directory = os.path.join('./', directory)
            os.makedirs(directory, exist_ok=True)
        else:
            self.directory = './'
        # clear file that stores eigenvalues
        open(os.path.join(self.directory, 'eigenvalues.txt'), 'w').close()

    def plot_potential2d(self, potential, potential_e=None, suffix=None):
        if potential_e is not None:
            u_xyg = [[(potential(x, y, self.dev.gates_z)-potential_e(x, y, self.dev.gates_z))*au.Eh for x in self.xx] for y in self.yy]
            u_xyw = [[(potential(x, y, self.dev.layer_z)-potential_e(x, y, self.dev.layer_z))*au.Eh for x in self.xx] for y in self.yy]
            u_xz = [[(potential(x, self.dev.box_y_size/2, z)-potential_e(x, self.dev.box_y_size/2, z))*au.Eh for x in self.xx] for z in self.zz]
            u_yz = [[(potential(self.dev.box_x_size/2, y, z)-potential_e(self.dev.box_x_size/2, y, z))*au.Eh for y in self.yy] for z in self.zz]
        else:   
            u_xyg = [[potential(x, y, self.dev.gates_z)*au.Eh for x in self.xx] for y in self.yy]
            u_xyw = [[potential(x, y, self.dev.layer_z)*au.Eh for x in self.xx] for y in self.yy]
            u_xz = [[potential(x, self.dev.box_y_size/2, z)*au.Eh for x in self.xx] for z in self.zz]
            u_yz = [[potential(self.dev.box_x_size/2, y, z)*au.Eh for y in self.yy] for z in self.zz]
        fig, ((a0,a1),(a2,a3)) = plt.subplots(2,2)
        fig.set_size_inches(15, 7)
        linsty = (0,(5,5))
        a0.set_aspect('equal', adjustable='box')
        a0.set_xlabel('x (nm)')
        a0.set_ylabel('y (nm)')
        a0.set_xlim([0, self.dev.box_x_size*au.Ah])
        a0.set_ylim([0, self.dev.box_y_size*au.Ah])
        potgrid0 = a0.pcolormesh(self.x_points*au.Ah, self.y_points*au.Ah, np.flip(u_xyg, axis=(0,1)), cmap='bwr')
        fig.colorbar(potgrid0, ax=a0)
        patch = patches.Polygon([[self.dev.box_x_size*au.Ah/2,0],[self.dev.box_x_size*au.Ah/2,self.dev.box_y_size*au.Ah]], closed=False, linewidth=1, edgecolor='k', linestyle=linsty)
        a0.add_patch(patch)
        patch = patches.Polygon([[0,self.dev.box_y_size*au.Ah/2],[self.dev.box_x_size*au.Ah,self.dev.box_y_size*au.Ah/2]], closed=False, linewidth=1, edgecolor='k', linestyle=linsty)
        a0.add_patch(patch)
        a1.set_aspect('equal', adjustable='box')
        a1.set_xlabel('x (nm)')
        a1.set_ylabel('y (nm)')
        a1.set_xlim([0, self.dev.box_x_size*au.Ah])
        a1.set_ylim([0, self.dev.box_y_size*au.Ah])
        potgrid1 = a1.pcolormesh(self.x_points*au.Ah, self.y_points*au.Ah, np.flip(u_xyw, axis=(0,1)), cmap='bwr')
        cbar1 = fig.colorbar(potgrid1, ax=a1)
        cbar1.set_label(r'$\phi$ (mV)')
        patch = patches.Polygon([[self.dev.box_x_size*au.Ah/2,0],[self.dev.box_x_size*au.Ah/2,self.dev.box_y_size*au.Ah]], closed=False, linewidth=1, edgecolor='k', linestyle=linsty)
        a1.add_patch(patch)
        patch = patches.Polygon([[0,self.dev.box_y_size*au.Ah/2],[self.dev.box_x_size*au.Ah,self.dev.box_y_size*au.Ah/2]], closed=False, linewidth=1, edgecolor='k', linestyle=linsty)
        a1.add_patch(patch)
        a2.set_aspect('equal', adjustable='box')
        a2.set_xlabel('x (nm)')
        a2.set_ylabel('z (nm)')
        a2.set_xlim([0, self.dev.box_x_size*au.Ah])
        a2.set_ylim([0, self.dev.box_z_size*au.Ah])
        potgrid2 = a2.pcolormesh(self.x_points*au.Ah, self.z_points*au.Ah, np.flip(u_xz, axis=(0,1)), cmap='bwr', vmin=-5000.)
        fig.colorbar(potgrid2, ax=a2)
        patch = patches.Polygon([[0,self.dev.layer_z*au.Ah],[self.dev.box_x_size*au.Ah,self.dev.layer_z*au.Ah]], closed=False, linewidth=1, edgecolor='k', linestyle=linsty)
        a2.add_patch(patch)
        patch = patches.Polygon([[0,self.dev.gates_z*au.Ah],[self.dev.box_x_size*au.Ah,self.dev.gates_z*au.Ah]], closed=False, linewidth=1, edgecolor='k', linestyle=linsty)
        a2.add_patch(patch)
        a3.set_aspect('equal', adjustable='box')
        a3.set_xlabel('y (nm)')
        a3.set_ylabel('z (nm)')
        a3.set_xlim([0, self.dev.box_x_size*au.Ah])
        a3.set_ylim([0, self.dev.box_z_size*au.Ah])
        potgrid3 = a3.pcolormesh(self.y_points*au.Ah, self.z_points*au.Ah, np.flip(u_yz, axis=(0,1)), cmap='bwr', vmin=-5000.)
        cbar3 = fig.colorbar(potgrid3, ax=a3)
        cbar3.set_label(r'$\phi$ (mV)')
        patch = patches.Polygon([[0,self.dev.layer_z*au.Ah],[self.dev.box_x_size*au.Ah,self.dev.layer_z*au.Ah]], closed=False, linewidth=1, edgecolor='k', linestyle=linsty)
        a3.add_patch(patch)
        patch = patches.Polygon([[0,self.dev.gates_z*au.Ah],[self.dev.box_x_size*au.Ah,self.dev.gates_z*au.Ah]], closed=False, linewidth=1, edgecolor='k', linestyle=linsty)
        a3.add_patch(patch)
        # label
        labels = ['(a)','(b)','(c)','(d)']
        for label, ax in zip(labels, fig.axes):
            # label physical distance in and down:
            trans = mtransforms.ScaledTranslation(10/72, -5/72, fig.dpi_scale_trans)
            ax.text(0.0, 1.0, label, transform=ax.transAxes + trans, fontsize='large', verticalalignment='top')
        #
        filename = 'potential2d'
        if suffix is not None:
            filename += suffix
        plt.savefig(os.path.join(self.directory, filename + '.png'), bbox_inches='tight', dpi=200)

    def plot_potential1d(self, potential, potential_e=None, suffix=None):
        # dump solution along symmetry axes
        if potential_e is not None:
            u_x = [(potential(x, self.dev.box_y_size/2, self.dev.layer_z)-potential_e(x, self.dev.box_y_size/2, self.dev.layer_z))*au.Eh for x in self.x_points]
            u_y = [(potential(self.dev.box_x_size/2, y, self.dev.layer_z)-potential_e(self.dev.box_x_size/2, y, self.dev.layer_z))*au.Eh for y in self.y_points]
        else:
            u_x = [potential(x, self.dev.box_y_size/2, self.dev.layer_z)*au.Eh for x in self.x_points]
            u_y = [potential(self.dev.box_x_size/2, y, self.dev.layer_z)*au.Eh for y in self.y_points]
        u0 = potential(self.dev.box_x_size/2, self.dev.box_y_size/2, self.dev.layer_z)*au.Eh
        u_yt = [(np.exp(-(y-35./au.Ah)**2/(65./au.Ah)**2/2.)-1.)*-1600. + u0 for y in self.y_points]
        u_yt1 = [(np.exp(-(y-35./au.Ah)**2/(70./au.Ah)**2/2.)-1.)*-1600. + u0 for y in self.y_points]
        # and plot
        fig, (a1,a2) = plt.subplots(2)
        fig.suptitle('potential energy along axes: y = 35. and x = 400.')
        a1.set_xlabel('x (nm)')
        a1.set_ylabel(r'$|e|\phi$ (mV)')
        a1.plot(self.x_points*au.Ah, u_x)
        a2.set_xlabel('y (nm)')
        a2.set_ylabel(r'$|e|\phi$ (mV)')
        a2.set_xlim([0, 70])
        a2.set_ylim([-17000, 2000])
        a2.plot(self.y_points*au.Ah, u_y)
        a2.plot(self.y_points*au.Ah, u_yt, 'g--')
        #a2.plot(self.y_points*au.Ah, u_yt1, 'g--')
        filename = 'potential1d'
        if suffix is not None:
            filename += suffix
        plt.savefig(os.path.join(self.directory, filename + '.png'), bbox_inches='tight', dpi=200)
        plt.close()








