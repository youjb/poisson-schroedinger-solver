import numpy as np

import poisson
import utils_confinement as utils_c
from utils_confinement import au
import schroedinger_tb
import utils as utils

import fenics as fn

#-------- device definition --------#
device = utils_c.DeviceParameters()
#
mesh_size = [device.mesh_x_size,device.mesh_y_size,device.mesh_z_size]  # [60,30,15]
mesh_refinement_rect = 1  # refinement z-range (in layer widths)
mesh_refinement_circ = 0./au.Ah  # radius of circular refinement next to the junction
mesh_refinement_shift = 0./au.Ah  # left and righ shift of the refinement circles 
mesher = poisson.Mesh(mesh_size, device, [mesh_refinement_rect, mesh_refinement_circ, mesh_refinement_shift])
mesh = mesher.generate_mesh()

vtkfile=fn.File('mesh.pvd')
vtkfile<<mesh

with open('gatelayout.npy', 'rb') as f:
    gatelayout = np.load(f)

gates = poisson.Gates(gatelayout, [3000./au.Eh, 3000./au.Eh, 3000./au.Eh, 3000./au.Eh, 
                                    2000./au.Eh, 0./au.Eh])

# initial guess for the electron charge
x, y = np.meshgrid(device.grid_x_space, device.grid_y_space)  # <- note reversed
sigmax, sigmay, mux, muy = 5./au.Ah, 5./au.Ah, 28./au.Ah, 28./au.Ah
arg = -(((x-mux)/sigmax)**2+((y-muy)/sigmay)**2)/2.
el_density = np.exp(arg)/sigmax/sigmay/np.pi/2
el_density[arg < -1.e2] = 0.  # avoid overflow in exp
#el_density[:] = 0.  # to calculate confinement without self-cosistency

#-------- Poisson part --------#
p_engine = poisson.Poisson_engine(device, mesh, gates, el_density)
p_engine.prepare_computational_box()
p_engine.prepare_solver()
p_engine.solve()
p_engine.solve(p_engine.u)

plotter = utils_c.Plotting([101,101,101], device, './results_confinement/')
plotter.plot_potential2d(p_engine.u, suffix="_t")
plotter.plot_potential1d(p_engine.u, suffix="_t")

# electron charge potential
p_engine_e = poisson.Poisson_engine(device, mesh, None, el_density)
p_engine_e.prepare_computational_box_e()
p_engine_e.prepare_solver()
p_engine_e.solve()
p_engine_e.solve(p_engine_e.u)

plotter.plot_potential2d(p_engine_e.u, suffix="_e")
plotter.plot_potential1d(p_engine_e.u, suffix="_e")
plotter.plot_potential2d(p_engine.u, p_engine_e.u, suffix="")
plotter.plot_potential1d(p_engine.u, p_engine_e.u, suffix="")




















