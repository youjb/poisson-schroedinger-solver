import fenics as fn
import numpy as np
from scipy import interpolate
# from numba import jit
import time

from utils_confinement import au


# numerical tolerance when defining geometrical domains
tol = 1.e-10

# material domain numbers
wse2_no = 1
hbn_no = 2
sio2_no = 3

# do the interpolation
def density_xy(device, density):
    return interpolate.interp2d(device.grid_x_space, device.grid_y_space, density, kind='cubic')

# define domains and boundaries
class Top_boundary(fn.SubDomain):

    def __init__(self, device, **kwargs):
        super().__init__(**kwargs) 
        self.dev = device
    def inside(self, x, on_boundary):
        return on_boundary and fn.near(x[2], self.dev.box_z_size, tol)
    
class Bottom_boundary(fn.SubDomain):

    def __init__(self, device, **kwargs):
        super().__init__(**kwargs) 
        self.dev = device
    def inside(self, x, on_boundary):
        return on_boundary and fn.near(x[2], 0, tol)

class Gate_geometry(fn.SubDomain):

    def __init__(self, device, mask, domain, **kwargs):
        super().__init__(**kwargs)   
        self.dev = device
        self.mask = mask        
        self.domain = domain
    def inside(self, x, on_boundary):
        present = self.mask[int(np.rint(x[0]/self.dev.box_x_size*(self.mask.shape[0]-1))), 
                            int(np.rint(x[1]/self.dev.box_y_size*(self.mask.shape[1]-1)))] == self.domain
        return fn.near(x[2], self.dev.gates_z, tol) and present

class Layer(fn.SubDomain):

    def __init__(self, device, **kwargs):
        super().__init__(**kwargs) 
        self.dev = device
    def inside(self, x, on_boundary):
        z_criterion = x[2] <= self.dev.layer_z + self.dev.layer_width/2 + tol
        z_criterion = z_criterion and x[2] >= self.dev.layer_z - self.dev.layer_width/2 - tol
        return z_criterion

class Top_barrier(fn.SubDomain):

    def __init__(self, device, **kwargs):
        super().__init__(**kwargs) 
        self.dev = device
    def inside(self, x, on_boundary):
        z_criterion = x[2] < self.dev.gates_z - tol
        z_criterion = z_criterion and x[2] > self.dev.layer_z + self.dev.layer_width/2 + tol
        return z_criterion

class Bottom_barrier(fn.SubDomain):

    def __init__(self, device, **kwargs):
        super().__init__(**kwargs)
        self.dev = device
    def inside(self, x, on_boundary):
        z_criterion = x[2] < self.dev.layer_z - self.dev.layer_width/2 - tol
        z_criterion = z_criterion and x[2] > 0. + tol
        return z_criterion

# define space-dependent permittivity
class Permittivity(fn.UserExpression):

    def __init__(self, markers, device, **kwargs):
        super().__init__(**kwargs)
        self.markers = markers
        self.dev = device
    def value_shape(self):
        return ()
    def eval_cell(self, values, x, cell):
        if self.markers[cell.index] == wse2_no:
            values[0] = self.dev.ewse2
        elif self.markers[cell.index] == hbn_no:
            values[0] = self.dev.ehbn
        elif self.markers[cell.index] == sio2_no:
            values[0] = self.dev.esio2
        else:
            values[0] = 1.

# define class for adding charge density
class Rho(fn.UserExpression):

    def __init__(self, density, device, **kwargs):
        super().__init__(**kwargs)
        self.density_xy = density
        self.dev = device
    def value_shape(self):
        return ()
    def gaussian_z(self, x):
        arg = -(((x[2]-self.dev.layer_z)/self.dev.sigma_z)**2)/2.
        if arg < -1.e2: return 0. # to avoid underflow in exp
        return np.exp(arg)/self.dev.sigma_z/np.sqrt(np.pi*2)
    def eval_cell(self, values, x, cell):
        values[0] = self.density_xy(x[0], x[1])*self.gaussian_z(x)

class TrueTopBoundaryE(fn.UserExpression):
    def __init__(self, device, density, **kwargs):
        super().__init__(**kwargs)
        self.dev = device
        self.density = density.T
    def coulomb(self, x, y):
        z2 = (self.dev.box_z_size-self.dev.layer_z)**2
        s = 0.
        for i,xi in enumerate(self.dev.grid_x_space):
            for j,yj in enumerate(self.dev.grid_y_space):
                s += self.density[i,j]/np.sqrt((x-xi)**2+(y-yj)**2+z2)
        s *= self.dev.grid_dx*self.dev.grid_dy/self.dev.ewse2
        return s
    def eval(self, values, x):
        values[0] = self.coulomb(x[0], x[1])
    def value_shape(self):
        #return (1,)
        return ()

# class to define the mesh
class Mesh:

    def __init__(self, size, device, refine_params):  
        self.size = size 
        self.dev = device
        self.refine_rect = refine_params[0]
        self.refine_circ = refine_params[1]
        self.refine_shift = refine_params[2]
    # utils to refine the mesh
    def refine_rectangular(self, mesh, x, y, z, w, h, l):
        cell_markers = fn.MeshFunction("bool", mesh, mesh.topology().dim())
        cell_markers.set_all(False)
        for cell in fn.cells(mesh):
            vertices = cell.get_vertex_coordinates()
            vertex = np.mean(vertices[0::3])
            vertey = np.mean(vertices[1::3])
            vertez = np.mean(vertices[2::3])
            if vertex < x+w/2 and vertex > x-w/2 and vertey < y+h/2 and vertey > y-h/2 and vertez < z+l/2 and vertez > z-l/2:
                cell_markers[cell] = True
        refined_mesh = fn.refine(mesh, cell_markers)
        return refined_mesh
    def refine_circular(self, mesh, x, y, z, r):
        cell_markers = fn.MeshFunction("bool", mesh, mesh.topology().dim())
        cell_markers.set_all(False)
        for cell in fn.cells(mesh):
            if cell.distance(fn.Point(x, y, z)) < r:
                cell_markers[cell] = True
        refined_mesh = fn.refine(mesh, cell_markers)
        return refined_mesh
    # engine to generate the mash
    def generate_mesh(self):
        mesh = fn.BoxMesh(fn.Point(0.0, 0.0, 0.0), 
                          fn.Point(self.dev.box_x_size, self.dev.box_y_size, self.dev.box_z_size),
                          self.size[0],
                          self.size[1],
                          self.size[2])
        # Refine the mesh nearby the monolayers
        mesh = self.refine_rectangular(mesh, 
                                       self.dev.box_x_size/2, 
                                       self.dev.box_y_size/2, 
                                       self.dev.layer_z, 
                                       self.dev.box_x_size, 
                                       self.dev.box_y_size, 
                                       self.dev.layer_width*self.refine_rect)
        # Refine the mesh nearby the gates
        mesh = self.refine_rectangular(mesh, 
                                       self.dev.box_x_size/2, 
                                       self.dev.box_y_size/2, 
                                       self.dev.gates_z, 
                                       self.dev.box_x_size, 
                                       self.dev.box_y_size, 
                                       self.dev.layer_width*self.refine_rect)
        # Refine the mesh around the center of the charge distribution
        '''
        mesh = self.refine_circular(mesh, 
                                    self.dev.box_x_size/2 - self.refine_shift, 
                                    self.dev.box_y_size/2, 
                                    self.dev.layer_z, 
                                    self.refine_circ)
        '''
        return mesh

# to store voltages and gates' layout mask  
class Gates:

    def __init__(self, mask, voltages):  
        self.volt = voltages
        self.mask = mask
        self.topvoltage = voltages[-2]
        self.backvoltage = voltages[-1]

# Poisson solver
class Poisson_engine:

    def __init__(self, device, mesh, gates, density):
        self.dev = device
        self.mesh = mesh
        self.gates = gates
        self.density = density
    def prepare_computational_box(self):
        # create the fem function base
        self.V = fn.FunctionSpace(self.mesh, 'CG', 1)
        # create subdomains and boundaries
        tbarrier = Top_barrier(self.dev)
        monolayer = Layer(self.dev)
        bbarrier = Bottom_barrier(self.dev)
        # assign material to domains
        materials = fn.MeshFunction("size_t", self.mesh, self.mesh.topology().dim(), 0)
        materials.set_all(1) # vacuum
        tbarrier.mark(materials, hbn_no)  # hbn
        monolayer.mark(materials, wse2_no)  # wse2
        bbarrier.mark(materials, sio2_no)  # sio2
        # assign conditions to boundaries
        gate1 = fn.DirichletBC(self.V, 
                               fn.Constant(self.gates.volt[0]), 
                               Gate_geometry(self.dev, self.gates.mask, 1))
        gate2 = fn.DirichletBC(self.V, 
                               fn.Constant(self.gates.volt[1]), 
                               Gate_geometry(self.dev, self.gates.mask, 2))
        gate3 = fn.DirichletBC(self.V, 
                               fn.Constant(self.gates.volt[2]), 
                               Gate_geometry(self.dev, self.gates.mask, 3))
        gate4 = fn.DirichletBC(self.V, 
                               fn.Constant(self.gates.volt[3]), 
                               Gate_geometry(self.dev, self.gates.mask, 4))
        top_bc = fn.DirichletBC(self.V, 
                                fn.Constant(self.gates.topvoltage), 
                                Top_boundary(self.dev))
        bottom_bc = fn.DirichletBC(self.V, 
                                   fn.Constant(self.gates.backvoltage), 
                                   Bottom_boundary(self.dev))
        self.bcs = [gate1, gate2, gate3, gate4, top_bc, bottom_bc]
        # define element measure, permittivity function, and charge density
        self.dx = fn.Measure('dx', domain=self.mesh, subdomain_data=materials)
        self.epsilon = Permittivity(materials, self.dev)
        self.charge_density = Rho(density_xy(self.dev, self.density), self.dev, degree=0)
    def prepare_computational_box_e(self):
        # create the fem function base
        self.V = fn.FunctionSpace(self.mesh, 'CG', 1)
        materials = fn.MeshFunction("size_t", self.mesh, self.mesh.topology().dim(), 0)
        materials.set_all(wse2_no)
        # neumann on all sides leads to problems with convergence
        true_top = TrueTopBoundaryE(self.dev, self.density)
        top_bc = fn.DirichletBC(self.V, true_top, Top_boundary(self.dev))  
        bottom_bc = fn.DirichletBC(self.V, fn.Constant(0.), Bottom_boundary(self.dev))  
        self.bcs = [top_bc, bottom_bc]
        # define element measure, permittivity function, and charge density
        self.dx = fn.Measure('dx', domain=self.mesh, subdomain_data=materials)
        self.epsilon = Permittivity(materials, self.dev)
        self.charge_density = Rho(density_xy(self.dev, self.density), self.dev, degree=0)
    def prepare_solver(self):
        # prepare problem
        u = fn.TrialFunction(self.V)
        v = fn.TestFunction(self.V)
        a = fn.dot(fn.grad(u), fn.grad(v))*self.epsilon*self.dx
        L = fn.Constant(4.*np.pi)*v*self.charge_density*self.dx
        self.A = fn.assemble(a)
        self.b = fn.assemble(L)
        for bc in self.bcs: bc.apply(self.A, self.b)
        # prepare solver
        fn.parameters['krylov_solver']['nonzero_initial_guess'] = True
        fn.parameters['krylov_solver']['monitor_convergence'] = True
        fn.parameters['krylov_solver']['absolute_tolerance'] = 1.e-12
        fn.parameters['krylov_solver']['relative_tolerance'] = 1.e-10
        fn.parameters['krylov_solver']['maximum_iterations'] = 1000
        self.solver = fn.KrylovSolver('gmres', 'ilu')  # 'hypre_euclid'
        fn.parameters['allow_extrapolation'] = True
    def solve(self, u=None):
        if u is None: u = fn.Function(self.V)
        self.solver.solve(self.A, u.vector(), self.b)
        self.u = u























