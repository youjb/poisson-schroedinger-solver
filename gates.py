import numpy as np
from skimage.draw import polygon

# length+width<len_QD/2
len_QD=70
width=6
length=28

gatelayout = np.zeros((int(len_QD)+1,int(len_QD)+1), dtype=np.int8)

gl = np.array([[0,len_QD/2-width],[length,len_QD/2-width], [length,len_QD/2+width], [0,len_QD/2+width]])
gr = np.array([[len_QD-length,len_QD/2-width],[len_QD,len_QD/2-width], [len_QD,len_QD/2+width], [len_QD-length,len_QD/2+width]])
gu = np.array([[len_QD/2-width,len_QD-length],[len_QD/2+width,len_QD-length], [len_QD/2+width,len_QD], [len_QD/2-width,len_QD]])
gd = np.array([[len_QD/2-width,0],[len_QD/2+width,0], [len_QD/2+width,length], [len_QD/2-width,length]])

gl1, gl2 = polygon(gl[:,0], gl[:,1], shape=gatelayout.shape)
gatelayout[gl1, gl2] = 1

gr1, gr2 = polygon(gr[:,0], gr[:,1], shape=gatelayout.shape)
gatelayout[gr1, gr2] = 2

gu1, gu2 = polygon(gu[:,0], gu[:,1], shape=gatelayout.shape)
gatelayout[gu1, gu2] = 3

gd1, gd2 = polygon(gd[:,0], gd[:,1], shape=gatelayout.shape)
gatelayout[gd1, gd2] = 4

with open('gatelayout.npy', 'wb') as f:
    np.save(f, gatelayout)

from PIL import Image
im = Image.fromarray(np.uint8((gatelayout.T) * 60))
im.save("gatelayout.png")

