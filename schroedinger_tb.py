import utils as utils

import numpy as np
from scipy import interpolate

class Schroedinger: 
    """
    Build singe-electron t-b model.

    """
    def __init__(self, flake_edge_size, material, device, **kwargs):
        self.flake_edge_size = flake_edge_size
        self.m = material
        self.dev = device
        self.bmag = 0.

    def define_flake(self):
        """
        define flake lattice
        """    
        self.flake = utils.Flake(self.m.a0, self.flake_edge_size, shape='rhombus', find_neighbours=False)
        self.flake.set_bfield(0.)  # in teslas
        # build reciprocal lattice
        self.flake.set_lattice_parameters(utils.Lattice())
        self.flake.create_reciprocal_lattice()
        # plot
        self.plot_flake = utils.PlottingOnFlake(self.flake, directory='results')
        self.plot_flake.plot_flake_lattice(plot_links=False)

    def load_potential_on_flake(self, potential, potential_e, return_efield=False, plot=False, flip_axes=False):
        """
        get confinement potential on the flake nodes
        """
        xx = self.dev.save_grid_x_space
        yy = self.dev.save_grid_y_space
        #
        fi_flake = np.array([[(potential(x, y, self.dev.layer_z)-potential_e(x, y, self.dev.layer_z)) for y in yy] for x in xx])
        fp = np.array([[(potential(x, y, self.dev.layer_z+self.dev.layer_width)-potential_e(x, y, self.dev.layer_z+self.dev.layer_width)) for y in yy] for x in xx])           
        fm = np.array([[(potential(x, y, self.dev.layer_z-self.dev.layer_width)-potential_e(x, y, self.dev.layer_z-self.dev.layer_width)) for y in yy] for x in xx])           
        efield_flake = (fp - fm)/self.dev.layer_width/2.
        #
        ps = utils.LoadPotential(self.dev)
        nodes = self.flake.nodes
        if flip_axes is True:
            nodes = self.flake.nodes[:,::-1]
        self.flake.potential = ps.get_values_at_points(fi_flake, nodes) 
        self.flake.electric_field = ps.get_values_at_points(efield_flake, nodes)
        #
        if plot:
            self.plot_flake.plot_potential_flake(self.flake.potential)
            self.plot_flake.plot_electric_field_flake(self.flake.electric_field)
        if return_efield:
            return self.flake.potential, self.flake.electric_field
        else:
            return self.flake.potential

    def plot_potential_flake(self, suffix=None):
        self.plot_flake.plot_potential_flake(self.flake.potential, suffix=suffix)

    def update_potential(self, potential):
        self.flake.potential = potential

    def build_flake_model(self, k_radius):
        """
        build flake model and construct k-space Hamiltonian
        """
        print("constructing plane-wave basis...")
        self.flake_model = utils.FlakeModel(self.flake, self.m)
        self.basis_k = utils.Planewaves(self.flake, self.flake_model)
        self.basis_k.select_subspace(self.flake.K_points, k_radius)
        self.plot_flake.plot_flake_lattice_k(subsets=self.basis_k.subspaces)
        # to be used later:
        self.flake_solver = utils.FlakeMethods(self.flake, self.m, basis_k=self.basis_k)

    def buld_plane_waves_basis(self, no_of_bands=None, energy_offset=0.):
        """
        build plane-waves basis and calculate matrix elements
        """
        self.basis_k.build_basis(no_of_bands=no_of_bands, energy_offset=energy_offset)
        #self.plot_flake.plot_energy_surface_k(subsets=self.basis_k.subspaces, energy=self.basis_k.basis_energies[::2])

    def calculate_potenetial_elements(self, potenetial_sign=1):  
        self.basis_k.potential_elements(sign=potenetial_sign)

    def build_hamiltonian(self):
        return self.basis_k.build_hamiltonian()

    def get_density_at_device_grid(self, eigenvectors, flip_axes=False):
        self.density_flake, _, _, _, _ = self.flake_solver.calculate_densities_k(eigenvectors[:,:-1],
                                                                                calculate_spin_valley=False, 
                                                                                calculate_real_states=False, 
                                                                                every_n=1)
        if flip_axes is False:
            nx = np.rint(self.flake.nodes[::2,0]/self.dev.grid_dx+self.dev.grid_x_dim/2)
            nx[nx < 0] = 0
            nx[nx > self.dev.grid_x_dim-1] = self.dev.grid_x_dim-1
            ny = np.rint(self.flake.nodes[::2,1]/self.dev.grid_dy+self.dev.grid_y_dim/2)
            ny[ny < 0] = 0
            ny[ny > self.dev.grid_y_dim-1] = self.dev.grid_y_dim-1
            nyx = np.stack((ny,nx), axis=1).astype(int)
            el_density = np.zeros((self.dev.grid_y_dim, self.dev.grid_x_dim))
        else:
            nx = np.rint(self.flake.nodes[::2,1]/self.dev.grid_dx+self.dev.grid_x_dim/2)
            nx[nx < 0] = 0
            nx[nx > self.dev.grid_x_dim-1] = self.dev.grid_x_dim-1
            ny = np.rint(self.flake.nodes[::2,0]/self.dev.grid_dy+self.dev.grid_y_dim/2)
            ny[ny < 0] = 0
            ny[ny > self.dev.grid_y_dim-1] = self.dev.grid_y_dim-1
        nxy = np.stack((nx,ny), axis=1).astype(int)
        el_density = np.zeros((self.dev.grid_x_dim, self.dev.grid_y_dim)) 
        #           
        for [nx,ny], dens in zip(nxy, self.density_flake[0,::2]):
            el_density[nx,ny] = dens
        return el_density.T  # <- reversed order

    def plot_results(self, eigenvalues, eigenvectors, whichstate):
        """
        present eigenproblem results
        """
        densities, spin_valley, _, _, _ = self.flake_solver.calculate_densities_k(eigenvectors,
                                                                                calculate_spin_valley=True, 
                                                                                calculate_real_states=False, 
                                                                                every_n=1)
        #self.plot_flake.plot_eigenvalues(eigenvalues)
        #self.plot_flake.plot_eigenvalues_sv(eigenvalues, spin_valley, pointsize=20.)
        self.plot_flake.plot_statedensity(densities[whichstate])
        #flake_solver.save_densities()

    def plot_density_flake(self, suffix=None):
        self.plot_flake.plot_statedensity(self.density_flake[0], suffix=suffix)
